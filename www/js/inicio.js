/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
document.addEventListener('deviceready',onDeviceReady, false);
 function onDeviceReady()
 {
	SqlServer.init("10.44.0.123", "SERVIDOR", "sa", "tinchocushina1615", "BDApp", function(event) 
	{	
		//conexion exitosa
	}, function(error) {
	  //error de conexion
	  alert("Error al conectar con el Host.Error: "+JSON.stringify(error));
	});
	 
	var app = angular.module('cordova', ["ngRoute"]);
		app.controller('apacheCtr', ['$scope', function($scope) {
			$scope.nombreUsuario =window.sessionStorage.getItem("nombreUsuario");
		}]);


		app.config(function($routeProvider) {
		  $routeProvider
			.when("/clickButtom", {
			  controller : "apacheCtr",
			  templateUrl: 'estadoCuenta.html'
			})
		});

		angular.bootstrap(document, ['cordova']);	
 }