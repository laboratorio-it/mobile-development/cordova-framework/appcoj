# appCoj

## Introduction

> La app "appCoj" permite resume de compras de un socio del C.O.J. y el estado actual de crédito, operatoria muy similar a la de una tarjeta de crédito.

## Installation
Ejecutar los siguientes comandos:

npm install
npm prepare
cordova platform add android

> Para utilizar el proyecto deberá cumplir con los siguientes requisitos:
- Sistema operativo: 	microsoft windows.
- Version de Cordova:	7.0.1
- Version de NPM:		3.10.10
- Version de Node:		v6.10.2